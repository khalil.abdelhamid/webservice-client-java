/**
 * Module.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package beans;

public class Module  implements java.io.Serializable {
    private java.lang.String cour;

    private java.lang.Integer idMatiere;

    private java.lang.String prof;

    public Module() {
    }

    public Module(
           java.lang.String cour,
           java.lang.Integer idMatiere,
           java.lang.String prof) {
           this.cour = cour;
           this.idMatiere = idMatiere;
           this.prof = prof;
    }


    /**
     * Gets the cour value for this Module.
     * 
     * @return cour
     */
    public java.lang.String getCour() {
        return cour;
    }


    /**
     * Sets the cour value for this Module.
     * 
     * @param cour
     */
    public void setCour(java.lang.String cour) {
        this.cour = cour;
    }


    /**
     * Gets the idMatiere value for this Module.
     * 
     * @return idMatiere
     */
    public java.lang.Integer getIdMatiere() {
        return idMatiere;
    }


    /**
     * Sets the idMatiere value for this Module.
     * 
     * @param idMatiere
     */
    public void setIdMatiere(java.lang.Integer idMatiere) {
        this.idMatiere = idMatiere;
    }


    /**
     * Gets the prof value for this Module.
     * 
     * @return prof
     */
    public java.lang.String getProf() {
        return prof;
    }


    /**
     * Sets the prof value for this Module.
     * 
     * @param prof
     */
    public void setProf(java.lang.String prof) {
        this.prof = prof;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Module)) return false;
        Module other = (Module) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cour==null && other.getCour()==null) || 
             (this.cour!=null &&
              this.cour.equals(other.getCour()))) &&
            ((this.idMatiere==null && other.getIdMatiere()==null) || 
             (this.idMatiere!=null &&
              this.idMatiere.equals(other.getIdMatiere()))) &&
            ((this.prof==null && other.getProf()==null) || 
             (this.prof!=null &&
              this.prof.equals(other.getProf())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCour() != null) {
            _hashCode += getCour().hashCode();
        }
        if (getIdMatiere() != null) {
            _hashCode += getIdMatiere().hashCode();
        }
        if (getProf() != null) {
            _hashCode += getProf().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Module.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enitie", "Module"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cour");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enitie", "cour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idMatiere");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enitie", "idMatiere"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prof");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enitie", "prof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
