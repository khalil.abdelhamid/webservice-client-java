package services;

public class ModuleServiceProxy implements services.ModuleService {
  private String _endpoint = null;
  private services.ModuleService moduleService = null;
  
  public ModuleServiceProxy() {
    _initModuleServiceProxy();
  }
  
  public ModuleServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initModuleServiceProxy();
  }
  
  private void _initModuleServiceProxy() {
    try {
      moduleService = (new services.ModuleServiceServiceLocator()).getModuleService();
      if (moduleService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)moduleService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)moduleService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (moduleService != null)
      ((javax.xml.rpc.Stub)moduleService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public services.ModuleService getModuleService() {
    if (moduleService == null)
      _initModuleServiceProxy();
    return moduleService;
  }
  
  public beans.Module addModule(beans.Module mat) throws java.rmi.RemoteException{
    if (moduleService == null)
      _initModuleServiceProxy();
    return moduleService.addModule(mat);
  }
  
  public beans.Module updateModule(int id, beans.Module mat) throws java.rmi.RemoteException{
    if (moduleService == null)
      _initModuleServiceProxy();
    return moduleService.updateModule(id, mat);
  }
  
  public boolean deleteModule(int id) throws java.rmi.RemoteException{
    if (moduleService == null)
      _initModuleServiceProxy();
    return moduleService.deleteModule(id);
  }
  
  public beans.Module[] getmats() throws java.rmi.RemoteException{
    if (moduleService == null)
      _initModuleServiceProxy();
    return moduleService.getmats();
  }
  
  
}