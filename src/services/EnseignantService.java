/**
 * ProfService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public interface EnseignantService extends java.rmi.Remote {
    public beans.Enseignant getEnseignant(java.lang.String cin) throws java.rmi.RemoteException;
    public beans.Enseignant updateProf(java.lang.String cin, beans.Enseignant professeur) throws java.rmi.RemoteException;
    public beans.Enseignant[] getprofesseurs() throws java.rmi.RemoteException;
    public beans.Enseignant addProf(beans.Enseignant professeur) throws java.rmi.RemoteException;
    public boolean deleteProf(java.lang.String cin) throws java.rmi.RemoteException;
}
