package services;

public class EnseignantServiceProxy implements EnseignantService {
  private String _endpoint = null;
  private EnseignantService enseignantService = null;
  
  public EnseignantServiceProxy() {
    _initProfServiceProxy();
  }
  
  public EnseignantServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initProfServiceProxy();
  }
  
  private void _initProfServiceProxy() {
    try {
      enseignantService = (new EnseignantLocator()).getProfService();
      if (enseignantService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub) enseignantService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub) enseignantService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (enseignantService != null)
      ((javax.xml.rpc.Stub) enseignantService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public EnseignantService getProfService() {
    if (enseignantService == null)
      _initProfServiceProxy();
    return enseignantService;
  }
  
  public beans.Enseignant getEnseignant(java.lang.String cin) throws java.rmi.RemoteException{
    if (enseignantService == null)
      _initProfServiceProxy();
    return enseignantService.getEnseignant(cin);
  }
  
  public beans.Enseignant updateProf(java.lang.String cin, beans.Enseignant professeur) throws java.rmi.RemoteException{
    if (enseignantService == null)
      _initProfServiceProxy();
    return enseignantService.updateProf(cin, professeur);
  }
  
  public beans.Enseignant[] getprofesseurs() throws java.rmi.RemoteException{
    if (enseignantService == null)
      _initProfServiceProxy();
    return enseignantService.getprofesseurs();
  }
  
  public beans.Enseignant addProf(beans.Enseignant professeur) throws java.rmi.RemoteException{
    if (enseignantService == null)
      _initProfServiceProxy();
    return enseignantService.addProf(professeur);
  }
  
  public boolean deleteProf(java.lang.String cin) throws java.rmi.RemoteException{
    if (enseignantService == null)
      _initProfServiceProxy();
    return enseignantService.deleteProf(cin);
  }
  
  
}