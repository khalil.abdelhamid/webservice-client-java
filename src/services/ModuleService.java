/**
 * ModuleService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public interface ModuleService extends java.rmi.Remote {
    public beans.Module addModule(beans.Module mat) throws java.rmi.RemoteException;
    public beans.Module updateModule(int id, beans.Module mat) throws java.rmi.RemoteException;
    public boolean deleteModule(int id) throws java.rmi.RemoteException;
    public beans.Module[] getmats() throws java.rmi.RemoteException;
}
