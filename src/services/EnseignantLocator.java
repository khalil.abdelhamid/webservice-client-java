/**
 * ProfServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public class EnseignantLocator extends org.apache.axis.client.Service implements EnseignantServiceService {

    public EnseignantLocator() {
    }


    public EnseignantLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public EnseignantLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ProfService
    private java.lang.String ProfService_address = "http://localhost:8080/server/services/ProfService";

    public java.lang.String getProfServiceAddress() {
        return ProfService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ProfServiceWSDDServiceName = "ProfService";

    public java.lang.String getProfServiceWSDDServiceName() {
        return ProfServiceWSDDServiceName;
    }

    public void setProfServiceWSDDServiceName(java.lang.String name) {
        ProfServiceWSDDServiceName = name;
    }

    public EnseignantService getProfService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ProfService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getProfService(endpoint);
    }

    public EnseignantService getProfService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            EnseignantServiceSoapBindingStub _stub = new EnseignantServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getProfServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setProfServiceEndpointAddress(java.lang.String address) {
        ProfService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (EnseignantService.class.isAssignableFrom(serviceEndpointInterface)) {
                EnseignantServiceSoapBindingStub _stub = new EnseignantServiceSoapBindingStub(new java.net.URL(ProfService_address), this);
                _stub.setPortName(getProfServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ProfService".equals(inputPortName)) {
            return getProfService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services", "ProfServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services", "ProfService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ProfService".equals(portName)) {
            setProfServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
