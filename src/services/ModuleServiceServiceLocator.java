/**
 * ModuleServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public class ModuleServiceServiceLocator extends org.apache.axis.client.Service implements services.ModuleServiceService {

    public ModuleServiceServiceLocator() {
    }


    public ModuleServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ModuleServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ModuleService
    private java.lang.String ModuleService_address = "http://localhost:8080/server/services/ModuleService";

    public java.lang.String getModuleServiceAddress() {
        return ModuleService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ModuleServiceWSDDServiceName = "ModuleService";

    public java.lang.String getModuleServiceWSDDServiceName() {
        return ModuleServiceWSDDServiceName;
    }

    public void setModuleServiceWSDDServiceName(java.lang.String name) {
        ModuleServiceWSDDServiceName = name;
    }

    public services.ModuleService getModuleService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ModuleService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getModuleService(endpoint);
    }

    public services.ModuleService getModuleService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            services.ModuleServiceSoapBindingStub _stub = new services.ModuleServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getModuleServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setModuleServiceEndpointAddress(java.lang.String address) {
        ModuleService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (services.ModuleService.class.isAssignableFrom(serviceEndpointInterface)) {
                services.ModuleServiceSoapBindingStub _stub = new services.ModuleServiceSoapBindingStub(new java.net.URL(ModuleService_address), this);
                _stub.setPortName(getModuleServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ModuleService".equals(inputPortName)) {
            return getModuleService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services", "ModuleServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services", "ModuleService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ModuleService".equals(portName)) {
            setModuleServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
