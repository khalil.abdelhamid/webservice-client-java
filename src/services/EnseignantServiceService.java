/**
 * ProfServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public interface EnseignantServiceService extends javax.xml.rpc.Service {
    public java.lang.String getProfServiceAddress();

    public EnseignantService getProfService() throws javax.xml.rpc.ServiceException;

    public EnseignantService getProfService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
