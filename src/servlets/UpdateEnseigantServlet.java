package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import beans.Enseignant;
import services.EnseignantService;
import services.EnseignantLocator;

/**
 * Servlet implementation class UpdateProf
 */
@WebServlet("/UpdateProf")
public class UpdateEnseigantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateEnseigantServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnseignantLocator wsdl = new EnseignantLocator();
		try {
			EnseignantService ps = wsdl.getProfService();
			String cne = request.getParameter("id");
			Enseignant prof = ps.getEnseignant(cne);
			request.setAttribute("prof", prof);
			 RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/UpdateEnseignant.jsp");
		     requestDispatcher.forward(request, response);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		EnseignantLocator wsdl = new EnseignantLocator();
		EnseignantService ps;
		try {
			ps = wsdl.getProfService();
			
			Enseignant enseignant = new Enseignant();
			enseignant.setCin(request.getParameter("cin"));
			enseignant.setNom(request.getParameter("nom"));
			enseignant.setPrenom(request.getParameter("prenom"));
			enseignant.setAge(Integer.parseInt(request.getParameter("age")));
			enseignant.setPhoto(request.getParameter("photo"));
			enseignant.setVille(request.getParameter("ville"));
			enseignant.setSexe(request.getParameter("sexe"));
			enseignant.setAdresse(request.getParameter("adresse"));
			ps.updateProf(enseignant.getCin(), enseignant);
			
			Enseignant[] enss =ps.getprofesseurs();
			request.setAttribute("ens", enss);
			
			 RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/ListAllEnseignant.jsp");
		     requestDispatcher.forward(request, response);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	
	}

}
