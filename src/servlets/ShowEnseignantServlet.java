package servlets;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Enseignant;
import services.EnseignantService;
import services.EnseignantLocator;

/**
 * Servlet implementation class DisplayProfs
 */
@WebServlet("/DisplayProfs")
public class ShowEnseignantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowEnseignantServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnseignantLocator wsdl = new EnseignantLocator();
		try {
			EnseignantService ps = wsdl.getProfService();
			
			Enseignant enseignant = new Enseignant();
			enseignant.setCin(request.getParameter("cin"));
			enseignant.setNom(request.getParameter("nom"));
			enseignant.setPrenom(request.getParameter("prenom"));
			enseignant.setAge(Integer.parseInt(request.getParameter("age")));
			enseignant.setPhoto(request.getParameter("photo"));
			enseignant.setVille(request.getParameter("ville"));
			enseignant.setSexe(request.getParameter("sexe"));
			enseignant.setAdresse(request.getParameter("adresse"));
			ps.addProf(enseignant);
			Enseignant[] enss =ps.getprofesseurs();
			Arrays.asList(enss).forEach(System.out::println);
			request.setAttribute("ens", enss);
			
			 RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/ListAllEnseignant.jsp");
		     requestDispatcher.forward(request, response);
			
			
		}catch(Exception e) {
			e.getStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
