package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Enseignant;
import services.EnseignantService;
import services.EnseignantLocator;

/**
 * Servlet implementation class DeleteProf
 */
@WebServlet("/DeleteProf")
public class DeleteEnseignantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteEnseignantServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnseignantLocator wsdl = new EnseignantLocator();
		try {
			EnseignantService ps = wsdl.getProfService();
			String cne = request.getParameter("id");
			ps.deleteProf(cne);
			Enseignant[] enss =ps.getprofesseurs();
			request.setAttribute("ens", enss);
			
			 RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/ListAllEnseignant.jsp");
		     requestDispatcher.forward(request, response);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
