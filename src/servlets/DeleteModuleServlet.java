package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.ModuleService;
import services.ModuleServiceServiceLocator;

/**
 * Servlet implementation class DeleteCour
 */
@WebServlet("/DeleteCour")
public class DeleteModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteModuleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ModuleServiceServiceLocator wsdl = new ModuleServiceServiceLocator();
		try {
			ModuleService ps = wsdl.getModuleService();
			int id = Integer.parseInt(request.getParameter("id"));
			ps.deleteModule(id);
		
			 RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/ListAllModules.jsp");
		     requestDispatcher.forward(request, response);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
