package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Enseignant;
import beans.Module;
import services.ModuleService;
import services.ModuleServiceServiceLocator;
import services.EnseignantService;
import services.EnseignantLocator;

/**
 * Servlet implementation class AjouterCour
 */
@WebServlet("/AjouterCour")
public class AddModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddModuleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		EnseignantLocator wsd = new EnseignantLocator();
		try {
			
			EnseignantService prof = wsd.getProfService();

			Enseignant[] enss =prof.getprofesseurs();
			request.setAttribute("ens", enss);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/AddModule.jsp");
		    requestDispatcher.forward(request, response);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		ModuleServiceServiceLocator wsdl = new ModuleServiceServiceLocator();
		
		try {
			
			ModuleService ps = wsdl.getModuleService();

			
			Module mdl = new Module();
			mdl.setCour(request.getParameter("cour"));
			mdl.setProf(request.getParameter("prof"));
		
			ps.addModule(mdl);
				
	
			beans.Module[] modules = ps.getmats();
			request.setAttribute("modules", modules);
			
			 RequestDispatcher requestDispatcher = request.getRequestDispatcher("pages/ListAllModules.jsp");
		     requestDispatcher.forward(request, response);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
