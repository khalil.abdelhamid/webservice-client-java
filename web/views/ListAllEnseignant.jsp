<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>


<body>

    <div class="container">
        <div class="row">
            <div class="row justify-content-center">
                <div class="col-6 mb-3">
                    <h1 class="align-self-center">Liste des professeurs</h1>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <table class="table table-smbordered table-lg">
                    <thead>
                        <tr>
                            <th>Cin</th>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>age</th>
                            <th>adresse</th>
                            <th>ville</th>
                            <th>sexe</th>
                            <td>Modifier</td>
                            <td>Suprimer</td>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="prof" items="${ens}">
                     <tr>
                                    <td><c:out value="${prof.cin}"></c:out></td>
                                    <td><c:out value="${prof.nom}" /></td>
                                    <td><c:out value="${prof.prenom }" /></td>
                                    <td><c:out value="${prof.age }"/></td>
                                    <td><c:out value="${prof.adresse }"/></td>
                                    <td><c:out value="${prof.ville}" /></td>
                                    <td><c:out value="${prof.sexe }"/></td>
                                    <td>
                                        <a type="button" class="btn btn-primary" href="UpdateProf?id=<c:out value="${prof.cin}"/>">Modifier</a>
                                    </td>
                                    <td>
                                        <a type="button" class="btn btn-danger" href="DeleteProf?id=<c:out value="${prof.cin}"/>">DELETE</a>
                                    </td>
                                </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</body>

</html>